let baseURL = '';
switch (process.env.NODE_ENV){
	case 'development': {
		baseURL = 'https://cnodejs.org/api/v1'
		break
	}
	default: {
		baseURL = 'https://cnodejs.org/api/v1'
	}
}

export {
	baseURL
}