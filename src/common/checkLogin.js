/*
  检查用户是否登录，根据access_token验证
  此方法，只用于小程序
*/
const checkLogin = () => {
	const access_token = uni.getStorageSync('access_token'),
	      { route } = getCurrentPages()[0]
	if(!access_token){
		uni.setStorageSync('to_path', route)
		uni.navigateTo({
			url: '/pages/login/index'
		})
		return false
	}
	return true
}

export default checkLogin