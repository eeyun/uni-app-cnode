/*
   直接根据access_token判断，用户是否登录
*/
export default () => {
	const access_token = uni.getStorageSync('access_token')
	return !!access_token
}