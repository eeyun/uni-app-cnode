/*
  工具类
*/

/*
  时间格式转换
  params: dateStr 
*/ 
export const formatDate = (dateStr) => {
	if(!dateStr) return dateStr;
	const now = new Date(),
		  year = now.getFullYear(),
		  month = now.getMonth(),
	      date = now.getDate();
	const fmNow = new Date(dateStr),
		  fmYear = fmNow.getFullYear(),
		  fmMonth = fmNow.getMonth(),
		  fmDate = fmNow.getDate();
	if(year === fmYear && month === fmMonth && date === fmDate){
		return '今天' + fmNow.getHours() + ':' + fmNow.getMinutes()
	}
	return fmYear + '-' + (fmMonth+1) + '-' + fmDate + ' ' + fmNow.getHours() + ':' + fmNow.getMinutes()
}

