import Vue from 'vue'
import App from './App'
import {router, RouterMount} from './router.js'
import store from './store'
import EventBus from './common/eventBus'
import request from './common/request'
import isLogin from './common/isLogin.js'
import checkLogin from './common/checkLogin.js'

Vue.use(router)

Vue.config.productionTip = false
Vue.prototype.$EventBus = EventBus
Vue.prototype.$request = request
Vue.prototype.$isLogin = isLogin
// #ifdef MP-WEIXIN
Vue.prototype.$checkLogin = checkLogin
// #endif

App.mpType = 'app'

const app = new Vue({
	store,
    ...App
})

//v1.3.5起 H5端 你应该去除原有的app.$mount();使用路由自带的渲染方式
// #ifdef H5
	RouterMount(app, router, '#app')
// #endif

// #ifndef H5
	app.$mount(); //为了兼容小程序及app端必须这样写才有效果
// #endif