import {RouterMount, createRouter} from 'uni-simple-router';

// 监控路由，判断用户是否登录
const checkLogin = (route, next) => {
	const { meta: { auth }, path } = route,
	      access_token = uni.getStorageSync('access_token')
	if(auth && !access_token){
		uni.setStorageSync('to_path', path)
		next({
			name: 'login',
			NAVTYPE: "push"
		})
	}else{
		next()
	}
}

const router = createRouter({
	platform: process.env.VUE_APP_PLATFORM,
	routes: [...ROUTES]
});

//全局路由前置守卫
router.beforeEach((to, from, next) => {
	checkLogin(to, next)
});

// 全局路由后置守卫
router.afterEach((to, from) => {
    console.log('跳转结束')
})

export {
	router,
	RouterMount
}