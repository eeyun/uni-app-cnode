import Vue from 'vue'
import VueX from 'vuex'

Vue.use(VueX)

/*
 *  命名方式：在state基础上添加前缀，便于后续区分/调用
 */

const store = new VueX.Store({
	state: {
		access_token: uni.getStorageSync('access_token') || '',
		user_message: uni.getStorageSync('user_msg') || {}
		
	},
	mutations: {
		// payload为用户传递的值，可以是单一值或者对象
		
		// 登录成功后，添加token
		add_access_token (state, payload){
			state.access_token = payload.token
		},
		
		// 登录成功后，添加用户基本信息
		add_user_message (state, payload){
			state.user_message = payload.user_mg
		}
		
	}
})

export default store
